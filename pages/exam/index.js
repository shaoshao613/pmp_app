//index.js
//获取应用实例
const app = getApp()
var Crypto = require('../../utils/cryptojs/cryptojs.js').Crypto;
var ti;
var that;
var order=1;
var q_prefix = "q_cn_";
var a_prefix = "result_";
var perlength = 16;
var money = 0;
var exam;
var queryOrder = 1;
var share = false;
var isError = false;
var mychoice = "A";
var errorList = [];
var isError = false;
var order_original = 1;
Page({
  data: {
    "id-B": "nothing",
    "id-C": "nothing",
    "id-D": "nothing",
    desc_height:"110px",
    hidden_next:"hidden",
    explain: "hidden",
    income:1,
    money:0,
    explain:""
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function (option) {
    that = this;
    exam = option.exam;
    queryOrder = option.order;
    share = option.share;
    order = 1;
    money = 0;
    errorList = [];
    isError = option.error == 1?true:false;
    console.log(option);
    var restart= option.restart;
    wx.getStorage({
      key: exam,
      success: function(res) {
        ti = res.data;
        wx.getStorage({
          key: exam+"_status",
          success: function(res) {
            wx.hideLoading();
            var data = res.data;
            console.log(exam)
            console.log("order"+order)
            console.log(data)
            if (!restart){
              order_original = data.order;
              order = order_original;
              money = data.money;
              errorList = data.errorList;
              if(isError && errorList){
                order = errorList.shift();
              }
            }
            showQuestion();
          },
          fail: function (res) {
            wx.hideLoading();
            order = 1;
            showQuestion();
          }
        })
      },
    })
    wx.showLoading({
      title: '数据加载中',
    });
    wx.request({
      url: 'https://tshao.sinaapp.com/pmp/data/' + exam +'_encode.data', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        wx.hideLoading();
        var mode = new Crypto.mode.CBC(Crypto.pad.pkcs7);
        var eb = Crypto.util.base64ToBytes(res.data);
        var kb = Crypto.charenc.UTF8.stringToBytes("1234567812345678");//KEY
        var vb = Crypto.charenc.UTF8.stringToBytes("1234567812345678");//IV
        var ub = Crypto.AES.decrypt(eb, kb, { iv: vb, mode: mode, asBpytes: true });
        ti = JSON.parse(ub);
        showQuestion();
        wx.setStorage({
          key: exam,
          data: ti,
        })
      }
    })
  },
  changeLang:function(){
    if (q_prefix == "q_cn_"){
      q_prefix = "q_en_";
      perlength = 32;
    }else{
      q_prefix = "q_cn_";
      perlength = 16;
    }
    showQuestion();
  },
  
  nextq:function(){
    order++;
    if(isError){
      order = errorList.shift();
    }
    console.log(errorList);
    showQuestion();
  },
  onShareAppMessage: function () {
    return {
      title: 'PMP宝典',
      desc: '这道题的答案为什么是这个啊？',
      path: '/pages/exam/index?exam='+exam+'&order='+order+"&share=1"
    }
  },
  choose:function(e){
    var choice = e.currentTarget.dataset.choice;
    mychoice = choice;
    console.log(choice);
    var result_item = ti['data'][a_prefix + order] ;
    showResult(choice, result_item);
    money = money + (result_item.result == choice ? 1 : 0);
    if(result_item.result!=choice){
      errorList.push(order);
    }
    if(!isError){
      order_original = order;
    }
    wx.setStorage({
      key: exam+"_status",
      data:{
        order:order_original+1,
        money:money,
        errorList:errorList,
      }
    });
  }

  
});
var showQuestion = function() {
  that.setData({
    question: ti['data'][q_prefix + order],
    hidden_next: "hidden",
    hidden_explain: "hidden",
    title:"第"+order+"题",
    desc_height: (ti['data'][q_prefix + order].description.length / perlength * 20 + 20)+ "px",
    id_A:"",
    id_B: "",
    id_C: "",
    id_D: "",
  })
  if (share) {
    order = queryOrder;
    showResult(mychoice, ti['data'][a_prefix + order]);
  }
};

var showResult = function (choice, result_item) {
  var result = result_item.result;
  that.setData({
    id_A: "A" == choice || "A" == result ? ("A" == result ? "right" : "wrong") : "nothing",
    id_B: "B" == choice || "B" == result ? ("B" == result ? "right" : "wrong") : "nothing",
    id_C: "C" == choice || "C" == result ? ("C" == result ? "right" : "wrong") : "nothing",
    id_D: "D" == choice || "D" == result ? ("D" == result ? "right" : "wrong") : "nothing",
    hidden_next: "",
    hidden_explain: "",
    explain_height: (result_item.description.length / 16 * 20 + 20) + "px",
    explain: result_item.description,
  })
};