//index.js
//获取应用实例
const app = getApp()
var that;
var exam_status=[];
var util = require('../../utils/util.js')
Page({
  data: {
    title: 'PMP考试要过关，模拟题库最重要\nBUG反馈，PMP培训团购价\n欢迎咨询微信：shaoshao613',
    userInfo: {},
    number_1:0,
    number_2: 0,
    number_3: 0,
    number_4: 0,
    number_5: 0,
    pmp_logo:"../../resources/pmp_logo.png",
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onShow: function () {
    that = this;
    wx.getStorage({
      key: "exam_1_status",
      success: function(res) {
        console.log(res.data);
        exam_status['exam_1'] = res.data;
        that.setData({
          number_1:res.data.order,
          error_1: res.data.errorList.length
        });
      },
    });
    wx.getStorage({
      key: "exam_2_status",
      success: function (res) {
        console.log(res.data);

        exam_status['exam_2'] = res.data;
        that.setData({
          number_2: res.data.order,
          error_2: res.data.errorList.length
        });
      },
    });
    wx.getStorage({
      key: "exam_3_status",
      success: function (res) {
        console.log(res.data);
        exam_status['exam_3'] = res.data;
        that.setData({
          number_3: res.data.order,
          error_3:res.data.errorList.length
        });
      },
    });
    wx.getStorage({
      key: "exam_4_status",
      success: function (res) {
        console.log(res.data);
        exam_status['exam_4'] = res.data;
        that.setData({
          number_4: res.data.order,
          error_4: res.data.errorList.length
        });
      },
    });
    wx.getStorage({
      key: "exam_5_status",
      success: function (res) {
        console.log(res.data);
        exam_status['exam_5'] = res.data;
        that.setData({
          number_5: res.data.order,
          error_5: res.data.errorList.length
        });
      },
    });

  },
  start_exam:function(e){
    var id = e.currentTarget.dataset.id;
    if (exam_status[id]){
      wx.showModal({
        title: '提示',
        content: '是否继续考试,点击取消重新开始',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '../exam/index?exam=' + id,
            })
          } else if (res.cancel) {
            wx.navigateTo({
              url: '../exam/index?exam=' + id + "&restart=1",
            })
          }
        }
      })
    }else{
      wx.navigateTo({
            url: '../exam/index?exam='+id,
          });
    }
    
  },

  start_error: function (e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../exam/index?exam=' + id+"&error=1",
    });
    

  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
